function typeCheck(object) {
    for(let [key,value] of Object.entries(object)){
		if(key.includes('string') && typeof value != 'string'){
			return `${value} is not String`;
		}
		else if(key.includes('int') && !Number.isInteger(value)){
			return `${value} is not Interger`;
		}
		else if(key.includes('float') && Number.isInteger(value)){
			return `${value} is not Float`;
		}
		else if(key.includes('bool') && typeof value != 'boolean'){
			return `${value} is not Boolean`;
		}
		else{
				object[key]=value;
			}
	}
	const validation = {
		set(object,key,val){
			if(key.includes('string') && typeof val != 'string'){
				console.log(`${val} is not String`);
			}
			else if(key.includes('int') && !Number.isInteger(val)){
				console.log(`${val} is not Interger`);
			}
			else if(key.includes('float') && Number.isInteger(val)){
				console.log(`${val} is not Float`);
			}
			else if(key.includes('bool') && typeof val != 'boolean'){
				console.log(`${val} is not Boolean`);
			}
			else{
				object[key]=val;
			}
		}
	}
	return new Proxy(object,validation)
}
const user = {
    age_int: 2,
    name_string: 'John',
	mark_float:9.2,
    job: 'Developer'
};

const checkedUserValidation = typeCheck(user);
checkedUserValidation.age_int = 2.1; // Throws error
checkedUserValidation.age_int = 2;
checkedUserValidation.job = 'fireman';
checkedUserValidation.address_string = 20; // Throws error

const employee = { employed_bool: 'true' };
const checkedEmployeeValidation = typeCheck(employee); // Throws error
console.log(checkedEmployeeValidation);
console.log(checkedUserValidation);

